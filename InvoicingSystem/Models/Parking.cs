﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using InvoicingSystem.Models;

namespace InvoicingSystem.Models
{
    public class Parking
    {
        #region Fields and Properties

        public int ParkingId { get; set; }

        public Person ParkingCustomer { get; set; }

        public ParkingHouse ParkingHouse { get; set; } = ParkingHouse.FindParkingHouseById(1);

        [DisplayName("Parking started")]
        public DateTime ParkingStartTime { get; set; }

        [DisplayName("Parking ended")]
        public DateTime ParkingEndTime { get; set; }

        #endregion


        #region Populating List

        public static List<Parking> parkingsListFull = new List<Parking>
        {
            new Parking //To check parking during day
            {
                ParkingId = 1,
                ParkingCustomer = Person.FindPersonById(1),
                ParkingStartTime = DateTime.Parse("2018.02.04 9:15"),
                ParkingEndTime = DateTime.Parse("2018.02.04 12:45")
            },
            new Parking //To check different month
            {
                ParkingId = 2,
                ParkingCustomer = Person.FindPersonById(1),
                ParkingStartTime = DateTime.Parse("2018.01.15 22:15"),
                ParkingEndTime = DateTime.Parse("2018.01.15 23:30")
            },
            new Parking //To check parking during night
            {
                ParkingId = 3,
                ParkingCustomer = Person.FindPersonById(1),
                ParkingStartTime = DateTime.Parse("2018.02.22 3:00"),
                ParkingEndTime = DateTime.Parse("2018.02.22 5:36"),
            },
            new Parking //To check parking during different nights but same period
            {
                ParkingId = 4,
                ParkingCustomer = Person.FindPersonById(1),
                ParkingStartTime = DateTime.Parse("2018.02.09 22:30"),
                ParkingEndTime = DateTime.Parse("2018.02.10 1:30")
            },
            new Parking //To check parking during another year
            {
                ParkingId = 5,
                ParkingCustomer = Person.FindPersonById(1),
                ParkingStartTime = DateTime.Parse("2017.02.26 13:15"),
                ParkingEndTime = DateTime.Parse("2017.02.26 17:20")
            },
            new Parking //To check user selection
            {
                ParkingId = 6,
                ParkingCustomer = Person.FindPersonById(4),
                ParkingStartTime = DateTime.Parse("2018.02.04 7:00"),
                ParkingEndTime = DateTime.Parse("2018.02.04 19:00")
            },
            new Parking //To check parking from day to night same day
            {
                ParkingId = 7,
                ParkingCustomer = Person.FindPersonById(1),
                ParkingStartTime = DateTime.Parse("2018.02.17 15:00"),
                ParkingEndTime = DateTime.Parse("2018.02.17 20:00")
            },
            new Parking //To check from night to day same day
            {
                ParkingId = 8,
                ParkingCustomer = Person.FindPersonById(1),
                ParkingStartTime = DateTime.Parse("2018.02.01 03:00"),
                ParkingEndTime = DateTime.Parse("2018.02.01 08:00")
            }
        };

        #endregion
        

        #region Methods

        public static Parking FindParkingById(int? id)
        {
            if (parkingsListFull.Find(x => x.ParkingId == id) != null)
            {
                return parkingsListFull.Where(x => x.ParkingId == id).Take(1).SingleOrDefault();
            }

            return null;
        }

        public static List<Parking> FindParkingByUserId(int? id)
        {
            if (parkingsListFull.Where(x => x.ParkingCustomer.UserId == id) != null)
            {
                return parkingsListFull.Where(x => x.ParkingCustomer.UserId == id).ToList();
            }

            return null;
        }      

        #endregion
    }
}