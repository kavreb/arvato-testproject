﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InvoicingSystem.Models
{
    public class Person
    {
        #region Fields and Properties
        [Key]
        public int UserId { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        //In the future, UserIdCode will be used to make sure one couldn't register a double user;
        //Might use Email instead.
        //We use string to better handle the long code.
        [DisplayName("Identification Code")]
        public string UserIdCode { get; set; } 

        [DisplayName("Customer status")]
        public bool UserIsCustomer { get; set; } = true;

        [DisplayName("Premium status")]
        public bool UserIsPremium { get; set; } = false;

        public bool UserIsOwner { get; set; } = false;

        public bool UserIsAdmin { get; set; } = false;

        #endregion


        #region Populating List

        public static List<Person> PeopleList = new List<Person>
        {
            new Person { UserId = 1,
                         FirstName = "Mare",
                         LastName = "Toonela",
                         UserIdCode = "48906070684",
                         UserIsPremium = true
            },
            new Person { UserId = 2,
                         FirstName = "Nele",
                         LastName = "Danielnova",
                         UserIdCode = "47612126587"
            },
            new Person { UserId = 3,
                         FirstName = "Toomas",
                         LastName = "Valiija",
                         UserIdCode = "39012110695"
            },
            new Person { UserId = 4,
                         FirstName = "Sipser",
                         LastName = "McCat",
                         UserIdCode = "49805070981",
                         UserIsPremium = true
            }
        };

        #endregion


        #region Methods

        public static void ChangeStatus(int id)
        {
            if (FindPersonById(id).UserIsPremium)
            {
                FindPersonById(id).UserIsPremium = false;
            }
            else
            {
                FindPersonById(id).UserIsPremium = true;
            }            
        }

        public static Person FindPersonById(int id)
        {
            if (PeopleList.Find(x => x.UserId == id) != null)
            {
                return PeopleList.Where(x => x.UserId == id).Take(1).SingleOrDefault();
            }

            return null;
        }
        #endregion
    }
}