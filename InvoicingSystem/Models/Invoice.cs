﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace InvoicingSystem.Models
{
    public class InvoiceParkingViewModel
    {
        public Invoice InvoiceObject { get; set; }
        public List<Parking> ParkingObject { get; set; }
    }

    public class Invoice
    {
        #region Fields and Properties

        [DisplayName("Invoice Nr")]
        public int InvoiceId { get; set; }

        [DisplayName("Invoice for User")]
        public Person InvoiceUser { get; set; } = Person.FindPersonById(1);

        [DisplayName("Invoice Period")]
        public string InvoicePeriod { get; set; }

        [DisplayName("Total Invoice")]
        public decimal TotalInvoiceAmount { get; set; }

        [DisplayName("Invoice creation date")]
        public DateTime DateInvoiceCreated { get; set; }

        #endregion


        #region Collections & populating

        public static List<Invoice> InvoicesList = new List<Invoice>
        {
            new Invoice
            {
                InvoiceId = 1,
                InvoiceUser = Person.FindPersonById(1),
                InvoicePeriod = "January",
                DateInvoiceCreated = DateTime.Parse("2018/03/08 19:00"),
                TotalInvoiceAmount = 22.25M
            }
        };

        #endregion


        #region Methods
        public static Invoice FindInvoiceById(int? id)
        {
            if (InvoicesList.Find(x => x.InvoiceId == id) != null)
            {
                return InvoicesList.Where(x => x.InvoiceId == id).Take(1).SingleOrDefault();
            }

            return null;
        }

        #endregion
    }
}