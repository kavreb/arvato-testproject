﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace InvoicingSystem.Models
{
    public class ParkingHouse
    {
        #region Fields and Properties

        public int ParkingHouseId { get; set; }

        [DisplayName("Parking House")]
        public string ParkingHouseName { get; set; }

        [DisplayName("Start time for day parking")]
        public DateTime DayStartTime { get; set; }

        [DisplayName("Start time for night parking")]
        public DateTime NightStartTime { get; set; }

        [DisplayName("The duration of one parking segment")]
        public decimal ParkingSegmentDuration { get; set; } = 30M;

        [DisplayName("Regular parking fee during the day")]
        public decimal RegularDayFee { get; set; } = 1.50M;

        [DisplayName("Regular parking fee during the night")]
        public decimal RegularNightFee { get; set; } = 1.00M;

        [DisplayName("Parking fee during the day for premium customer")]
        public decimal PremiumDayFee { get; set; } = 1.00M;

        [DisplayName("Parking fee during the night for premium customer")]
        public decimal PremiumNightFee { get; set; } = 0.75M;
        
        [DisplayName("Premium Monthly Fee")]
        public decimal PremiumMonthlyFee { get; set; } = 20M;

        [DisplayName("Premium Invoice Cap")]
        public decimal PremiumInvoiceCap { get; set; } = 300M;

        #endregion
        

        #region Populating

        public static List<ParkingHouse> ParkingHousesList = new List<ParkingHouse>
        {
            new ParkingHouse {ParkingHouseId = 1,
                              ParkingHouseName = "Central Housing",
                              DayStartTime = DateTime.Parse("7:00"),
                              NightStartTime = DateTime.Parse("19:00"),
                              RegularDayFee = 1.50M,
                              RegularNightFee = 1.00M,
                              PremiumDayFee = 1.00M,
                              PremiumNightFee = 0.75M
            }
        };

        #endregion
        

        #region Methods

        public static ParkingHouse FindParkingHouseById(int id)
        {
            if (ParkingHousesList.Find(x => x.ParkingHouseId == id) != null)
            {
                return ParkingHousesList.Where(x => x.ParkingHouseId == id).Take(1).SingleOrDefault();
            }

            return null;
        }

        #endregion
    }
}