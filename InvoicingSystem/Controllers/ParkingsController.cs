﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using InvoicingSystem.Models;


namespace InvoicingSystem.Controllers
{
    public class ParkingsController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("ParkingsList");
        }

        public ActionResult ParkingsList()
        {
            var customer = Person.FindPersonById(1);

            IEnumerable<Parking> readResult = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59141/api/parkings");

                var responseTask = client.GetAsync("parkings");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Parking>>();
                    readTask.Wait();

                    readResult = readTask.Result;
                }
                else
                {
                    readResult = Enumerable.Empty<Parking>();

                    ModelState.AddModelError(string.Empty, "Server error.");
                }

            }

            return View(readResult.Where(x => x.ParkingCustomer.UserId == customer.UserId)
                                  .OrderBy(x => x.ParkingStartTime.Year)
                                  .ThenBy(x => x.ParkingStartTime.DayOfYear))
                                  ;
        }

        //Todo: Implement parking Post (Lauri)
        public ActionResult Create()
        {
            return View();
        }


        [System.Web.Mvc.HttpPost]
        public ActionResult Create(Parking parking)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59141/api/parkings");

                var postTask = client.PostAsJsonAsync<Parking>("parkings", parking);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("ParkingsList");
                }
            }

            ModelState.AddModelError(string.Empty, "Server error.");

            return View(parking);
        }
    }
}