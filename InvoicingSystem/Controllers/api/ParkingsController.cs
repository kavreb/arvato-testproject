﻿using InvoicingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace InvoicingSystem.Controllers.api
{
    public class ParkingsController : ApiController
    {
        // GET: api/Parkings
        public List<Parking> GetAllParkings()
        {
            return Parking.parkingsListFull;
        }

        // GET: api/Parkings/5
        [ResponseType(typeof(Parking))]
        public IHttpActionResult GetParking(int id)
        {
            Parking parking = Parking.FindParkingById(id);
            if (parking == null)
            {
                return NotFound();
            }

            return Ok(parking);
        }

        //TODO: Implement posting parkings (Lauri)
    }
}
