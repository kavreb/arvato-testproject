﻿using InvoicingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InvoicingSystem.Controllers.api
{
    public class InvoicesController : ApiController
    {
        // GET: api/Invoices
        public List<Invoice> GetAllInvoices()
        {
            return Invoice.InvoicesList;
        }

        // GET: api/Invoices/5
        public Invoice Get(int id)
        {
            return Invoice.FindInvoiceById(id);
        }

        // POST: api/Invoices
        [System.Web.Mvc.HttpPost]
        public IHttpActionResult Post(Invoice invoice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }

            //Todo: Add "if" check to avoid duplicate invoices of the same period (Lauri)

            Invoice.InvoicesList.Add(new Invoice()
            {
                InvoiceId = invoice.InvoiceId,
                InvoiceUser = invoice.InvoiceUser,
                InvoicePeriod = invoice.InvoicePeriod,
                DateInvoiceCreated = invoice.DateInvoiceCreated,
                TotalInvoiceAmount = invoice.TotalInvoiceAmount
            });

            return Ok("Invoice created");
        }
    }
}
