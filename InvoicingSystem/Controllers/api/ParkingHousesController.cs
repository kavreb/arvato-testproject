﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InvoicingSystem.Models;

namespace InvoicingSystem.Controllers
{
    public class ParkingHousesController : ApiController
    {
        // GET: api/ParkingHouses
        public List<ParkingHouse> GetAllParkingHouses()
        {
            return ParkingHouse.ParkingHousesList;
        }

        // GET: api/ParkingHouses/5
        public ParkingHouse Get(int id)
        {
            return ParkingHouse.FindParkingHouseById(id);
        }

        //TODO: Implement Views and Controller (Lauri)
    }
}
