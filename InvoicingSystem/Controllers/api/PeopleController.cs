﻿using InvoicingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InvoicingSystem.Controllers.api
{
    public class PeopleController : ApiController
    {
        // GET: api/People
        public List<Person> GetAllPeople()
        {
            return Person.PeopleList;
        }

        // GET: api/People/5
        public Person Get(int id)
        {
            return Person.FindPersonById(id);
        }

        public IHttpActionResult PostNewPerson(Person person)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }

            //Todo: Add "if" check to avoid duplicate users (Lauri)

            Person.PeopleList.Add(new Person()
            {
                UserId = person.UserId,
                FirstName = person.FirstName,
                LastName = person.LastName,
                UserIdCode = person.UserIdCode,
                UserIsPremium = person.UserIsPremium
            });
            
            return Ok("Person added");
        }
    }
}
