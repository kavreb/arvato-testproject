﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using InvoicingSystem.Models;


namespace InvoicingSystem.Controllers
{
    public class PeopleController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Customers");
        }

        public ActionResult Customers()
        {
            IEnumerable<Person> readResult = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59141/api/people");

                var responseTask = client.GetAsync("people");
                responseTask.Wait();

                var result = responseTask.Result;
                if(result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Person>>();
                    readTask.Wait();

                    readResult = readTask.Result; 
                }
                else
                {
                    readResult = Enumerable.Empty<Person>();

                    ModelState.AddModelError(string.Empty, "Server error.");
                }

            }

            return View(readResult.OrderBy(x => x.LastName));
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Create(Person person)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59141/api/People");

                var postTask = client.PostAsJsonAsync<Person>("people", person);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Customers");
                }
            }

            ModelState.AddModelError(string.Empty, "Server error.");

            return View(person);
        }
    }
}
