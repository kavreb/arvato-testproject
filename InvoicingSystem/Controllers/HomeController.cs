﻿using InvoicingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InvoicingSystem.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? id)
        {
            ViewBag.Title = "Home Page";
            
            if (id.HasValue)
            {
                int trueId = id.Value;
                Person.ChangeStatus(trueId);
            }

            ViewBag.User = Person.FindPersonById(1).FirstName + " " + Person.FindPersonById(1).LastName;
            ViewBag.UserId = Person.FindPersonById(1).UserId;

            if (Person.FindPersonById(1).UserIsPremium)
            {
                ViewBag.Status = "Premium";
            }
            else
            {
                ViewBag.Status = "Regular";
            }

            return View();
        }
    }
}
