﻿using InvoicingSystem.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace InvoicingSystem.Controllers
{
    public class InvoicesController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("InvoicesList");
        }


        // GET: Invoices/InvoicesList
        public ActionResult InvoicesList()
        {
            var customer = Person.FindPersonById(1);

            IEnumerable<Invoice> readResult = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59141/api/invoices");

                var responseTask = client.GetAsync("invoices");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Invoice>>();
                    readTask.Wait();

                    readResult = readTask.Result;
                }
                else
                {
                    readResult = Enumerable.Empty<Invoice>();

                    ModelState.AddModelError(string.Empty, "Server error.");
                }
            }

            return View(readResult.Where(x => x.InvoiceUser.UserId == customer.UserId)
                                  .OrderBy(x => x.InvoiceId))
                                  ;
        }


        // GET: Invoices/details/5
        public ActionResult Details(int? id)
        {
            //TODO: Do through api (readResult.Where(x => x.InvoiceUser.UserId == id).Take(1).SingleOrDefault ) (Lauri)

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Invoice activeInvoice = Invoice.FindInvoiceById(id);

            if (activeInvoice == null)
            {
                return HttpNotFound();
            }

            List<Parking> activeParking = Parking.FindParkingByUserId(1);

            var ipViewModel = new InvoiceParkingViewModel
            {
                InvoiceObject = activeInvoice,
                ParkingObject = activeParking
            };

            return View(ipViewModel);
        }


        // CREATE: Invoices/Create
        public ActionResult Create(string month)
        {
            //TODO: In the future, customer will be defined by who is logged in (Lauri)
            var customer = Person.FindPersonById(1);
            var customerParkings = new List<Parking>();
            string invoiceMonth = month;
            var invoiceYear = 2018; //TODO: Should be user input as well (Lauri)
            long totalParkingTimeDay = 0;
            long totalParkingTimeNight = 0;
            decimal totalFee = 0;
            var currentParkingHouse = ParkingHouse.FindParkingHouseById(1); //TODO: PH id should be dependent on user's parking (Lauri)

            foreach (var item in Parking.parkingsListFull.Where(p => p.ParkingCustomer.UserId == customer.UserId))
            {
                if (item.ParkingEndTime.ToString("MMMM") == invoiceMonth
                    && item.ParkingEndTime.Year == invoiceYear)
                {
                    customerParkings.Add(item);
                }
            }

            //TODO: Rework the parking times calculation method (Lauri)
            /* The following foreach calculates correctly the minutes for parkings that started during the same period
             * that they ended (e.g. user parked from 9am to 11am). However, at the moment all the minutes 
             * from the parking periods that crossed day-night borders are added to the counter of whichever period
             * they started in. Meaning if they started parking in the morning of day 1 and ended the night of day two,
             * they will pay the same price for both the day and night parking (in this case the day price).
             * 
             * The code could be modified to accomodate precise calculations (as can be seen from my experiments after around line 150),
             * but it may also prove to be necessary to rework the code, cutting all the parked times up into half-hour pieces
             * (or whatever else the parkinghouse uses as their segment),
             * and add them according to their start time to the night or day counter. It would be more accurate. However,
             * I became aware of it too late and so here we have this. (Lauri)
             */
            foreach (var item in customerParkings)
            {   //for when parking is during the day period of the same day
                if (item.ParkingStartTime.Hour >= currentParkingHouse.DayStartTime.Hour
                    && item.ParkingEndTime.Hour < currentParkingHouse.NightStartTime.Hour
                    && item.ParkingStartTime.Day == item.ParkingEndTime.Day)
                {
                    totalParkingTimeDay = totalParkingTimeDay + (item.ParkingEndTime - item.ParkingStartTime).Ticks;
                }
                //for night parking during same period (from 7pm one day to 7am the next day)
                else if ((item.ParkingStartTime.Hour >= currentParkingHouse.NightStartTime.Hour     //if starts after night time
                       || item.ParkingStartTime.Hour < currentParkingHouse.DayStartTime.Hour)       //or before morning
                       && (item.ParkingStartTime.Day == item.ParkingEndTime.Day                     //and it's the same day
                       && ((item.ParkingEndTime.Hour < currentParkingHouse.DayStartTime.Hour)       //(and if so, then ends before day start, meaning, is during night period,
                       || (item.ParkingStartTime.Hour >= currentParkingHouse.NightStartTime.Hour))  // or starts after night start, meaning is also night period) 
                       || (item.ParkingStartTime.Day == item.ParkingEndTime.Day - 1                 //or starts the previous day
                       && item.ParkingStartTime.Hour >= currentParkingHouse.NightStartTime.Hour)))  //and starts after previous day's night start (meaning will be only during night period)
                {
                    totalParkingTimeNight = totalParkingTimeNight + (item.ParkingEndTime - item.ParkingStartTime).Ticks;
                }
                else //for when parking is over different periods
                {
                    if (item.ParkingStartTime.Hour >= currentParkingHouse.DayStartTime.Hour
                        && item.ParkingStartTime.Hour < currentParkingHouse.NightStartTime.Hour)
                    {
                        totalParkingTimeDay = totalParkingTimeDay + (item.ParkingEndTime - item.ParkingStartTime).Ticks;
                    }
                    else
                    {
                        totalParkingTimeNight = totalParkingTimeNight + (item.ParkingEndTime - item.ParkingStartTime).Ticks;
                    }

                    //From here follow experiments for finding the true time of overlapping periods. For now they remain painfully incomplete and unworking.

                    //var currentDayStart = item.ParkingStartTime.Subtract(item.ParkingStartTime.TimeOfDay).Add(currentParkingHouse.DayStartTime.TimeOfDay);
                    //var currentNightStart = item.ParkingEndTime.Subtract(item.ParkingEndTime.TimeOfDay).Add(currentParkingHouse.NightStartTime.TimeOfDay);

                    ////To get the minutes of the first period
                    //if (item.ParkingStartTime.Hour < currentDayStart.Hour)
                    //{
                    //    totalParkingTimeNight = totalParkingTimeNight + (currentDayStart - item.ParkingStartTime).Ticks;
                    //}
                    ////else if (item.ParkingStartTime.Hour > currentNightStart.Hour)
                    ////{
                    ////    totalParkingTimeNight = totalParkingTimeNight + (item.ParkingStartTime.Date - item.ParkingStartTime).Ticks;
                    ////}
                    //else if (item.ParkingStartTime.Hour >= currentDayStart.Hour
                    //        && item.ParkingStartTime.Hour <= currentNightStart.Hour)
                    //{
                    //    totalParkingTimeDay = totalParkingTimeDay + (currentNightStart - item.ParkingStartTime).Ticks;
                    //}

                    ////To get the minutes of the last period
                    //if (item.ParkingEndTime.Hour < currentParkingHouse.DayStartTime.Hour)
                    //{
                    //    totalParkingTimeNight = totalParkingTimeNight + (item.ParkingEndTime - item.ParkingEndTime.AddDays(-1)).Ticks;
                    //}
                    //else if (item.ParkingEndTime.Hour > currentParkingHouse.NightStartTime.Hour)
                    //{
                    //    totalParkingTimeNight = totalParkingTimeNight + ((item.ParkingEndTime.Date - item.ParkingEndTime).Ticks - currentParkingHouse.NightStartTime.Ticks);
                    //}
                }

            }

            decimal totalParkingMinutesDay = Convert.ToDecimal(TimeSpan.FromTicks(totalParkingTimeDay).TotalMinutes);
            decimal totalParkingMinutesNight = Convert.ToDecimal(TimeSpan.FromTicks(totalParkingTimeNight).TotalMinutes);

            if (customer.UserIsPremium)
            {
                totalFee = (totalParkingMinutesDay / currentParkingHouse.ParkingSegmentDuration * currentParkingHouse.PremiumDayFee)
                    + (totalParkingMinutesNight / currentParkingHouse.ParkingSegmentDuration * currentParkingHouse.PremiumNightFee)
                    + currentParkingHouse.PremiumMonthlyFee;

                if (totalFee > currentParkingHouse.PremiumInvoiceCap)
                {
                    totalFee = currentParkingHouse.PremiumInvoiceCap;
                }
            }
            else if (!customer.UserIsPremium)
            {
                totalFee = (totalParkingMinutesDay / currentParkingHouse.ParkingSegmentDuration * currentParkingHouse.RegularDayFee)
                    + (totalParkingMinutesNight / currentParkingHouse.ParkingSegmentDuration * currentParkingHouse.RegularNightFee);
            }

            totalFee = decimal.Round(totalFee, 2, MidpointRounding.AwayFromZero);

            ViewBag.TotalParkingTime = totalParkingMinutesDay.ToString();
            ViewBag.TotalParkingTimeNight = totalParkingMinutesNight.ToString();
            ViewBag.Fee = totalFee;
            ViewBag.CurrentMonth = month;
            ViewBag.CurrentId = Invoice.InvoicesList.LastOrDefault().InvoiceId + 1;

            List<string> monthNames = new List<string>();

            for (int i = 1; i <= DateTime.Now.Month; i++)
            {
                //Todo: Add not only months up to now, but all the months the user has parked during (Lauri)
                monthNames.Add(DateTimeFormatInfo.CurrentInfo.GetMonthName(i));
            }

            ViewBag.MonthNames = monthNames;

            return View();
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Create(Invoice invoice)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59141/api/Invoices");

                var postTask = client.PostAsJsonAsync<Invoice>("invoices", invoice);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("InvoicesList");
                }

                Invoice.InvoicesList.Add(invoice);
            }

            ModelState.AddModelError(string.Empty, "Server error.");

            return RedirectToAction("Details", new { id = invoice.InvoiceId });
        }
    }
}
